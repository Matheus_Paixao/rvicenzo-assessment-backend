const router = require('express').Router()
const route = '/'

module.exports = (server) => {
    router.get('/', (req, res) => {
        res.send("Hello world")
    })

    server.use(route, router)
}