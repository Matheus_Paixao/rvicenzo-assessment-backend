const { Category, Product } = require('../models')

class CategoryController {
    async all(req, res) {
        try {
            const categories = await Category.findAll({
                include: [
                    {
                        model: Product,
                        as: 'products',
                        through: { attributes: [] },
                    },
                ]
            })        

            return res.json(categories)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async find(req, res) {
        try {
            const { id } = req.params
            const category = await Category.findOne({
                where: {
                    id
                },
                include: [
                    {
                        model: Product,
                        as: 'products',
                        through: { attributes: [] },
                    },
                ]            
            })

            return res.json(category)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async create(req, res) {
        try {
            const { products, ...data } = req.body
	        const category = await Category.create(data)

            if (products && products.length > 0) {
                category.setProducts(products)
            }

            return res.status(201).json(category)		
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async update(req, res) {
        try {
            const { id } = req.params                  
            const { products, ...data } = req.body
            const category = await Category.findOne({
                where: { id }
            })
            
            category.update(data)
            
            if (products && products.length > 0) {
                category.setProducts(products)        
            }

            return res.json(category)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }

    async remove(req, res) {
        try {
            const { id } = req.params
            const category = await Category.findOne({
                where: { id }
            })
            
            category.destroy(id)

            return res.json(category)
        } catch (err) {
            return res.status(500).json({ err })
        }
    }
}

module.exports = new CategoryController()